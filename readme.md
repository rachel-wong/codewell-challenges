# Codewell Challenges

> A series of front-end only challenges to achieve pixel-perfect dimensions according to the design using only HTML, SCSS (BEM), Javascript where necessary.

To see the designs

1. `git clone`

2. Navigate to design folder (ie. /fiber)

3. Open Index.html on your favourite browser of choice or click "Go Live" on VSCode

## Designs Implemented so far

#### Fiber

https://sad-hopper-6e0f2a.netlify.app/fiber/index.html or /Fiber/index.html

![Fiber](/fiber/Design/fiber.gif)

#### Bookmark Landing

https://sad-hopper-6e0f2a.netlify.app/bookmark_landing/index.html

![Bookmark landing](/bookmark_landing/images/bookMark_screenshot.gif)

## Designs still in progress

/Timenow/index.html

## Spense

![Fiber](/Spense/Assets/firstPass_screen.gif)

https://sad-hopper-6e0f2a.netlify.app/spense/index.html or /Spense/index.html

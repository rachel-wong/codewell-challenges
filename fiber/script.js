const openButton = document.querySelector('[data-mob-open]')
const closeButton = document.querySelector('[data-mob-close]')
const mobileNav = document.querySelector('.nav__listwrapper')
console.log('mobileNav:', mobileNav)

openButton.addEventListener('click', () => {
  mobileNav.classList.add('active')
})

closeButton.addEventListener('click', () => {
  mobileNav.classList.remove('active')
})
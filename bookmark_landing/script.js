const accordion = document.querySelector('[data-accordion]')
const toggles = [...accordion.querySelectorAll('[data-accordion-toggle]')]

toggles.forEach((toggle, toggleIndex) => {
  toggle.addEventListener("click", () => {
    if (toggle.nextElementSibling.classList.contains('accordion__content')) {
      let contentDiv = toggle.nextElementSibling
      let icon = toggle.querySelector(".accordion__toggle__arrow")
      console.log('icon:', icon)
      if (parseInt(contentDiv.style.height) != contentDiv.scrollHeight) {
        contentDiv.style.height = contentDiv.scrollHeight + "px"
        icon.classList.add("active")
      } else {
        contentDiv.style.height = 0
        icon.classList.remove("active")
      }
    }
  })
})

let tabs = document.querySelectorAll("[data-tab-target]")
let content = document.querySelectorAll(".tab-pane")

// listen to each tab click
tabs.forEach(tab => {
  tab.addEventListener("click", () => {
    // get the content tab with the same tab label
    let targetContent = document.querySelector(tab.dataset.tabTarget)

    // close all content
    content.forEach(tabContent => {
      tabContent.classList.remove("active")
    })

    // but open the content that has the same tab label
    targetContent.classList.add("active")
  })
})

const openMobileNav = document.querySelector(".mobile-nav-trigger")
const closeMobileNav = document.querySelector(".mobile-close-btn")
const mobileNav = document.querySelector(".nav-list")

openMobileNav.addEventListener('click', () => {
  mobileNav.classList.add('active')
})

closeMobileNav.addEventListener('click', () => {
  mobileNav.classList.remove('active')
})